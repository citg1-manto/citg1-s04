package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * The HttpServletRequest and the HttpSerletResponse objects should imported from the jakarta package is the java enterprise edition package
	  * Changing the method name to "doPost" ensure that this will only be accessible via "Post" request
	  * Service method (doPost, service) - the second stage of the servlet lifecycle that handles the request and response
	  * service() method - handle any type of 
	 */
	
	private static final long serialVersionUID = 43283931101411089L;
	
	/*
	 * 
	 * Initialization - First Stage of the servlet life cycle
	 * the "init" method is used to perform function such as connecting to database and initializing values to be used in the context of the servlet
	 *  - load the servlet
	 *  - creates an instance of the servlet class
	 *  - initialize 
	 */
	public void init()throws ServletException{
		System.out.println("****************************");
		System.out.println("Initialize connection to database.");
		System.out.println("****************************");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		System.out.println("Hello from the calculator servlet.");
		
		/*
		 * 
		 * The parameter name are define in the form input field
		 * 
		 * 
		 */
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		int total =num1+num2;
		// the getWriter()method is used to print out information in the browser as response
		// getWriter get 
		PrintWriter out = res.getWriter();
		
		out.print("<h1>the total of the two numbers are: "+total+"</h1>");
		
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.print("<h1>You have accessed the get method of the calculator servlet.</h1>");
	}
	
	/*
	 * 
	 * Finalization - it is the last part of the servlet lifecycle that invokes the "destroy" method.
	 * - Clean up of the resources once the servlet is destroyed/unused.
	 * - Closing the connection.
	 */
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("Disconnected from database");
		System.out.println("***********************************");
	}
}
