package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4484146589817098769L;

	public void init() throws ServletException{
        System.out.println("******************************************");
        System.out.println(" RoomServlet has been initialized. ");
        System.out.println("******************************************");
    }
	
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
    	        
    	       //branding - servlet context parameter
    			ServletContext srvContext = getServletContext();
    			String branding = srvContext.getInitParameter("branding");
    			
    			//firstname - system properties
    			String firstname = System.getProperty("firstname");
    			
    			//lastname - httpSession
    			HttpSession session = req.getSession();
    			String lastname = session.getAttribute("lastname").toString();
    			
    			//Email - servlet context
    			String email = (String) srvContext.getAttribute("email");
    			
    			//Contacts - redirect
    			String contact = req.getParameter("contact");
    			
    			PrintWriter output = res.getWriter();
    			
    			output.println(
    					"<h1>"+branding+"</h1>"+
    					"<p>First name:  "+firstname+"</p>"+
    					"<p>Last name: "+lastname+"</p>"+
    					"<p>Contact : "+contact +"</p>"+
    					"<p>Email : "+email+"</p>"
    					);
	}
	
	 public void destroy(){
	        System.out.println("******************************************");
	        System.out.println(" RoomServlet has been destroyed. ");
	        System.out.println("******************************************");
	    }

}
