package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1804323570948502389L;
	public void init() throws ServletException{
        System.out.println("******************************************");
        System.out.println(" RoomServlet has been initialized. ");
        System.out.println("******************************************");
    }
	public void doPost(HttpServletRequest req, HttpServletResponse res ) throws IOException{
	
		//System properties
		System.getProperties().put("firstname",req.getParameter("firstname").toString());
		
		//HttpSession
		HttpSession session = req.getSession();
		session.setAttribute("lastname",req.getParameter("lastname"));
		
		//Servlet Context setAttribute method
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email",req.getParameter("email"));
		
		//url rewriting
		res.sendRedirect("detail?contact="+req.getParameter("contact"));
	}
	 public void destroy(){
	        System.out.println("******************************************");
	        System.out.println(" RoomServlet has been destroyed. ");
	        System.out.println("******************************************");
	    }
	
	
	
	
}
